package presentacion;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.filechooser.FileNameExtensionFilter;

import logica.DtCanal;
import logica.DtUsuario;
import logica.IControllerUsuario;
import logica.Tipo;

import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Date;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.JEditorPane;
import javax.swing.JRadioButton;

public class FrmAgregarUsuario extends JInternalFrame {
	private JTextField txtNickName;
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtCorreo;
	private JTextField txtNombreCanal;
	private JEditorPane txtDescripcion;
	private JRadioButton rdbtnPublico;
	private JRadioButton rdbtnPrivado;
	private Boolean checkImagenIngresada;
	private ButtonGroup radioGroup;
	private JButton btnImagen;
	
	/**
	 * @wbp.parser.constructor
	 */
	public FrmAgregarUsuario(IControllerUsuario iCtrlUsuario) {
		checkImagenIngresada = false;
		setResizable(true);
		setTitle("Agregar Usuario");
		setBounds(100, 100, 381, 520);
		setLocation(0, 0);
		
		JLabel lblIngreseLosSiguientes = new JLabel("Ingrese los siguientes datos");
		
		JPanel panelNick = new JPanel();
		
		JLabel lblNickname = new JLabel("NickName");
		lblNickname.setHorizontalAlignment(SwingConstants.CENTER);
		
		txtNickName = new JTextField();
		txtNickName.setHorizontalAlignment(SwingConstants.CENTER);
		txtNickName.setColumns(10);
		
		JPanel panelNombre = new JPanel();
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setHorizontalAlignment(SwingConstants.CENTER);
		
		txtNombre = new JTextField();
		txtNombre.setHorizontalAlignment(SwingConstants.CENTER);
		txtNombre.setColumns(10);
		
		JPanel panelApellido = new JPanel();
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setHorizontalAlignment(SwingConstants.CENTER);
		
		txtApellido = new JTextField();
		txtApellido.setHorizontalAlignment(SwingConstants.CENTER);
		txtApellido.setColumns(10);
		
		JPanel panelCorreo = new JPanel();
		
		JLabel lblCorreo = new JLabel("Correo");
		lblCorreo.setHorizontalAlignment(SwingConstants.CENTER);
		
		txtCorreo = new JTextField();
		txtCorreo.setHorizontalAlignment(SwingConstants.CENTER);
		txtCorreo.setColumns(10);
		
		JPanel panelFecha = new JPanel();
		
		JLabel lblFechaDeNacimiento = new JLabel("Fecha de Nacimiento");
		lblFechaDeNacimiento.setHorizontalAlignment(SwingConstants.CENTER);
		
		JPanel panelImagen = new JPanel();
		
		JLabel lblImagenDePerfil = new JLabel("Imagen de Perfil");
		lblImagenDePerfil.setHorizontalAlignment(SwingConstants.CENTER);
		
		btnImagen = new JButton("Seleccionar..");
		btnImagen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/* Programamos JFileChooser */
				
				JFileChooser jFile = new JFileChooser();
				FileNameExtensionFilter filtro = new FileNameExtensionFilter("JPG & PNG Images", "jpg","png");
				jFile.setFileFilter(filtro);
				int seleccion = jFile.showOpenDialog(btnImagen);
				if(seleccion==jFile.APPROVE_OPTION){
					File imagen = jFile.getSelectedFile();
					btnImagen.setText(imagen.getPath());
					checkImagenIngresada = true;
					
					
				}
				
			}
		});
		
		JPanel panelGuardar = new JPanel();
		
		JButton btnGuardar = new JButton("Guardar");
		
		JButton btnCancelar = new JButton("Cancelar");
		
		JPanel panel = new JPanel();
		
		JPanel panel_2 = new JPanel();
		
		JPanel panel_1 = new JPanel();
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(62)
					.addComponent(lblIngreseLosSiguientes, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE))
				.addComponent(panelNick, GroupLayout.DEFAULT_SIZE, 374, Short.MAX_VALUE)
				.addComponent(panelNombre, GroupLayout.DEFAULT_SIZE, 374, Short.MAX_VALUE)
				.addComponent(panelApellido, GroupLayout.DEFAULT_SIZE, 374, Short.MAX_VALUE)
				.addComponent(panelCorreo, GroupLayout.DEFAULT_SIZE, 374, Short.MAX_VALUE)
				.addComponent(panelFecha, GroupLayout.DEFAULT_SIZE, 374, Short.MAX_VALUE)
				.addComponent(panelImagen, GroupLayout.DEFAULT_SIZE, 374, Short.MAX_VALUE)
				.addComponent(panelGuardar, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 374, Short.MAX_VALUE)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 362, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 374, Short.MAX_VALUE)
				.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 374, GroupLayout.PREFERRED_SIZE)
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(lblIngreseLosSiguientes)
					.addGap(14)
					.addComponent(panelNick, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
					.addGap(5)
					.addComponent(panelNombre, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
					.addGap(3)
					.addComponent(panelApellido, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
					.addGap(3)
					.addComponent(panelCorreo, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
					.addGap(4)
					.addComponent(panelFecha, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
					.addComponent(panelImagen, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(panelGuardar, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE))
		);
		
		JLabel lblDescripcion = new JLabel("Descripcion");
		
		txtDescripcion = new JEditorPane();
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblDescripcion, GroupLayout.DEFAULT_SIZE, 92, Short.MAX_VALUE)
					.addGap(108)
					.addComponent(txtDescripcion, GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
					.addGap(0))
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(20)
							.addComponent(lblDescripcion))
						.addComponent(txtDescripcion, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panel_2.setLayout(gl_panel_2);
		
		JLabel lblPrivacidadDeCanal = new JLabel("Privacidad de Canal");
		
		rdbtnPublico = new JRadioButton("Publico");
		
		rdbtnPrivado = new JRadioButton("Privado");
		radioGroup = new ButtonGroup();
		radioGroup.add(rdbtnPrivado);
		radioGroup.add(rdbtnPublico);
		
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblPrivacidadDeCanal, GroupLayout.PREFERRED_SIZE, 139, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 69, Short.MAX_VALUE)
					.addComponent(rdbtnPrivado)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(rdbtnPublico))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblPrivacidadDeCanal)
						.addComponent(rdbtnPublico)
						.addComponent(rdbtnPrivado))
					.addContainerGap(9, Short.MAX_VALUE))
		);
		panel_1.setLayout(gl_panel_1);
		
		JLabel lblNombreCanal = new JLabel("Nombre Canal");
		
		txtNombreCanal = new JTextField();
		txtNombreCanal.setColumns(10);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNombreCanal)
					.addGap(128)
					.addComponent(txtNombreCanal, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNombreCanal)
						.addComponent(txtNombreCanal, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		GroupLayout gl_panelImagen = new GroupLayout(panelImagen);
		gl_panelImagen.setHorizontalGroup(
			gl_panelImagen.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelImagen.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblImagenDePerfil, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
					.addGap(90)
					.addComponent(btnImagen, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_panelImagen.setVerticalGroup(
			gl_panelImagen.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelImagen.createSequentialGroup()
					.addGap(10)
					.addGroup(gl_panelImagen.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnImagen)
						.addComponent(lblImagenDePerfil)))
		);
		panelImagen.setLayout(gl_panelImagen);
		GroupLayout gl_panelGuardar = new GroupLayout(panelGuardar);
		gl_panelGuardar.setHorizontalGroup(
			gl_panelGuardar.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelGuardar.createSequentialGroup()
					.addGap(81)
					.addComponent(btnGuardar, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnCancelar, GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE)
					.addGap(80))
		);
		gl_panelGuardar.setVerticalGroup(
			gl_panelGuardar.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelGuardar.createSequentialGroup()
					.addGap(5)
					.addGroup(gl_panelGuardar.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnGuardar)
						.addComponent(btnCancelar)))
		);
		panelGuardar.setLayout(gl_panelGuardar);
		
		JPanel panelSpinner = new JPanel();
		GroupLayout gl_panelFecha = new GroupLayout(panelFecha);
		gl_panelFecha.setHorizontalGroup(
			gl_panelFecha.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelFecha.createSequentialGroup()
					.addGap(12)
					.addComponent(lblFechaDeNacimiento, GroupLayout.PREFERRED_SIZE, 152, GroupLayout.PREFERRED_SIZE)
					.addGap(5)
					.addComponent(panelSpinner, GroupLayout.PREFERRED_SIZE, 193, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_panelFecha.setVerticalGroup(
			gl_panelFecha.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelFecha.createSequentialGroup()
					.addGroup(gl_panelFecha.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelFecha.createSequentialGroup()
							.addGap(7)
							.addComponent(lblFechaDeNacimiento))
						.addComponent(panelSpinner, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		JSpinner spinnerDia = new JSpinner();
		panelSpinner.add(spinnerDia);
		spinnerDia.setModel(new SpinnerNumberModel(1, 1, 31, 1));
		
		JSpinner spinnerMes = new JSpinner();
		panelSpinner.add(spinnerMes);
		spinnerMes.setModel(new SpinnerNumberModel(1, 1, 12, 1));
		
		JSpinner spinnerAnio = new JSpinner();
		panelSpinner.add(spinnerAnio);
		spinnerAnio.setModel(new SpinnerNumberModel(new Integer(1970), new Integer(1970), null, new Integer(1)));
		panelFecha.setLayout(gl_panelFecha);
		GroupLayout gl_panelCorreo = new GroupLayout(panelCorreo);
		gl_panelCorreo.setHorizontalGroup(
			gl_panelCorreo.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelCorreo.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblCorreo, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
					.addGap(169)
					.addComponent(txtCorreo, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_panelCorreo.setVerticalGroup(
			gl_panelCorreo.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelCorreo.createSequentialGroup()
					.addGap(7)
					.addGroup(gl_panelCorreo.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtCorreo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblCorreo)))
		);
		panelCorreo.setLayout(gl_panelCorreo);
		GroupLayout gl_panelApellido = new GroupLayout(panelApellido);
		gl_panelApellido.setHorizontalGroup(
			gl_panelApellido.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelApellido.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblApellido, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
					.addGap(160)
					.addComponent(txtApellido, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_panelApellido.setVerticalGroup(
			gl_panelApellido.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelApellido.createSequentialGroup()
					.addGap(7)
					.addGroup(gl_panelApellido.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtApellido, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblApellido)))
		);
		panelApellido.setLayout(gl_panelApellido);
		GroupLayout gl_panelNombre = new GroupLayout(panelNombre);
		gl_panelNombre.setHorizontalGroup(
			gl_panelNombre.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelNombre.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNombre, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
					.addGap(162)
					.addComponent(txtNombre, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_panelNombre.setVerticalGroup(
			gl_panelNombre.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelNombre.createSequentialGroup()
					.addGap(7)
					.addGroup(gl_panelNombre.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNombre)
						.addComponent(txtNombre, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
		);
		panelNombre.setLayout(gl_panelNombre);
		GroupLayout gl_panelNick = new GroupLayout(panelNick);
		gl_panelNick.setHorizontalGroup(
			gl_panelNick.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelNick.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNickname, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)
					.addGap(148)
					.addComponent(txtNickName, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_panelNick.setVerticalGroup(
			gl_panelNick.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelNick.createSequentialGroup()
					.addGap(5)
					.addGroup(gl_panelNick.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNickname)
						.addComponent(txtNickName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
		);
		panelNick.setLayout(gl_panelNick);
		getContentPane().setLayout(groupLayout);
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				limpiarDatos();
				spinnerDia.setValue(1);
				spinnerMes.setValue(1);
				spinnerAnio.setValue(1900);
				setVisible(false);
			}

		});
		
		
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(checkForm()){
					try {
						/*MANEJO DE LA IMAGEN */ 
						String imgUsuario = "";
						if(checkImagenIngresada){
							Path origen = Paths.get(btnImagen.getText());
							String rutaProyecto = System.getProperty("user.dir");
							rutaProyecto = rutaProyecto+"/img/";
							int cantImagenesEnDirectorio = new File(rutaProyecto).listFiles().length;
							imgUsuario = rutaProyecto + cantImagenesEnDirectorio;
							Path rutaDestinoYNombre = Paths.get(imgUsuario);
							Files.copy(origen,rutaDestinoYNombre,StandardCopyOption.REPLACE_EXISTING);
						}
						/* Manejo Dt's */
						int anio = (int)spinnerAnio.getValue();
						int mes = (int)spinnerMes.getValue();
						int dia = (int)spinnerDia.getValue();
						Date fechaNacimiento = new Date(anio, mes, dia);
						DtUsuario newUser = new DtUsuario(imgUsuario, fechaNacimiento, txtApellido.getText(),txtNombre.getText() , txtCorreo.getText(), txtNickName.getText());
						Tipo tipo = Tipo.PRIVADO;
						if(rdbtnPublico.isSelected()){
							tipo = Tipo.PUBLICO;
						}
						DtCanal newCanal = new DtCanal(txtNombreCanal.getText(), txtDescripcion.getText(), tipo);
						/* Operaciones con iControllerUsuario */
						
						iCtrlUsuario.insertarDataUser(newUser);
						iCtrlUsuario.insertarDataCanal(newCanal);
						iCtrlUsuario.darAltaUsuario();
					} catch (IOException e1) { }
					setVisible(false);
					limpiarDatos();
				}
			}
		});
	}
	/*
	 * 
	 * 
	 * Es tan solo una prueba.
	
	public FrmAgregarUsuario(int ancho, int alto){
		setBounds(100, 100, 450, 300);
		Double anchoPorcentaje = ancho*0.30;
		Double altoPorcentaje = alto*0.20;
		setLocation(anchoPorcentaje.intValue(),altoPorcentaje.intValue());
	}*/
	
	private void limpiarDatos() {
		txtNickName.setText("");
		txtApellido.setText("");
		txtCorreo.setText("");
		txtNombre.setText("");
		txtDescripcion.setText("");
		txtNombreCanal.setText("");
		radioGroup.clearSelection();
		btnImagen.setText("Seleccionar..");
		
	}

	private boolean checkForm(){
		String nickName = txtNickName.getText();
		String correo = txtCorreo.getText();
		Boolean radioBtnIsSelected = rdbtnPrivado.isSelected() || rdbtnPublico.isSelected();
		if(nickName.isEmpty() || correo.isEmpty() || !radioBtnIsSelected){
			JOptionPane.showMessageDialog(this, "No puede haber campos vacios","Agregar Usuario", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		else{
			return true;

		}
		
	}
}
