package presentacion;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;


import java.util.Set;
import javax.swing.JPanel;

import logica.Categoria;
import logica.DtCategoria;
import logica.DtListaVideoPropietario;
import logica.IControllerCategoria;

import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FormConsultaCategoria extends JInternalFrame {

	/**
	 * Launch the application.
	 */
	private IControllerCategoria iControllerCategoria;
	private JComboBox<DtCategoria> comboBoxCategoria;

	/**
	 * Create the frame.
	 */
	public FormConsultaCategoria( IControllerCategoria iCtrlCategoria) {
		setClosable(true);
		setMaximizable(true);
		setBounds(100, 100, 450, 473);
		iControllerCategoria=iCtrlCategoria;
		
		JPanel panel = new JPanel();
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 415, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(327, Short.MAX_VALUE))
		);
		
		JLabel lblSeleccionarCategoria = new JLabel("Seleccionar Categoria");
		
		
		comboBoxCategoria= new JComboBox<DtCategoria>();
		
		JButton btnSeleccionarCategoria = new JButton("Seleccionar");
		btnSeleccionarCategoria.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println(String.valueOf(comboBoxCategoria.getSelectedItem()));
				
			}
		});
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(5)
					.addComponent(lblSeleccionarCategoria)
					.addGap(22)
					.addComponent(comboBoxCategoria, GroupLayout.PREFERRED_SIZE, 199, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(54, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
					.addContainerGap(260, Short.MAX_VALUE)
					.addComponent(btnSeleccionarCategoria, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
					.addGap(40))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap(20, Short.MAX_VALUE)
					.addComponent(lblSeleccionarCategoria)
					.addGap(58))
				.addGroup(Alignment.LEADING, gl_panel.createSequentialGroup()
					.addGap(16)
					.addComponent(comboBoxCategoria, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnSeleccionarCategoria, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(7, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		getContentPane().setLayout(groupLayout);

	}
	  public void cargarCategorias() {
      	DefaultComboBoxModel<DtCategoria> model;
    	Set<DtCategoria> myset= iControllerCategoria.listarDtCategoria();
    	if (!myset.isEmpty()) {
    	   	DtCategoria[] nuevoSet = myset.toArray(new DtCategoria[myset.size()]);
            model = new DefaultComboBoxModel<DtCategoria>(nuevoSet);
            comboBoxCategoria.setModel(model);
            }
	    }
	  public void seleccionarCategoria(String nombreCategoria) {
		  iControllerCategoria.seleccionarCategoria(String.valueOf(comboBoxCategoria.getSelectedItem()));
		  Categoria category= iControllerCategoria.getCategoriaRecordada();
		  Set<DtListaVideoPropietario> dtListaVideo=category.obtenerListaVideoPropietario();
		  DefaultComboBoxModel<DtListaVideoPropietario> model;
		  if (!dtListaVideo.isEmpty()) {
			  DtListaVideoPropietario[] listaVideoPropietarios = dtListaVideo.toArray(new DtListaVideoPropietario[dtListaVideo.size()]);
	            model = new DefaultComboBoxModel<DtListaVideoPropietario>(listaVideoPropietarios);
	            
		  }
			
		}
		  
		  
		  
	
}
