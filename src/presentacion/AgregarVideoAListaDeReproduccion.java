package presentacion;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.JButton;

public class AgregarVideoAListaDeReproduccion extends JInternalFrame {
	/**
	 * Create the frame.
	 */
	public AgregarVideoAListaDeReproduccion() {
		setTitle("Agregar Video A Lista De Reproduccion");
		setBounds(100, 100, 758, 674);
		
		JLabel lblSeleccionarUsuario = new JLabel("Seleccionar Usuario");
		
		JComboBox comboBoxVideoDeUsuario = new JComboBox();
		
		JLabel lblVideosDelUsuario = new JLabel("Videos del Usuario");
		
		JComboBox comboBox = new JComboBox();
		
		JButton btnSeleccionarUsuario = new JButton("Seleccionar");
		
		JButton btnSeleccionarVideo = new JButton("Seleccionar");
		
		JInternalFrame internalFrameListasDeUsuario = new JInternalFrame("Agregar a la Lista");
		internalFrameListasDeUsuario.setVisible(true);
		
		JButton btnCancelarVideo = new JButton("Cancelar");
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblSeleccionarUsuario, GroupLayout.PREFERRED_SIZE, 161, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(582, Short.MAX_VALUE))
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblVideosDelUsuario)
					.addContainerGap(610, Short.MAX_VALUE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(43)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(btnSeleccionarVideo)
						.addComponent(comboBoxVideoDeUsuario, GroupLayout.PREFERRED_SIZE, 451, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(261, Short.MAX_VALUE))
				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
					.addGap(51)
					.addComponent(internalFrameListasDeUsuario, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(139, Short.MAX_VALUE))
				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
					.addGap(45)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnSeleccionarUsuario)
							.addContainerGap())
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(btnCancelarVideo, GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE)
								.addComponent(comboBox, 0, 464, Short.MAX_VALUE))
							.addGap(246))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblSeleccionarUsuario, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
					.addGap(12)
					.addComponent(btnSeleccionarUsuario)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblVideosDelUsuario)
					.addGap(18)
					.addComponent(comboBoxVideoDeUsuario, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
					.addGap(0, 0, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(18)
							.addComponent(btnSeleccionarVideo))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(45)
							.addComponent(btnCancelarVideo)))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(internalFrameListasDeUsuario, GroupLayout.PREFERRED_SIZE, 342, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		
		JLabel lblSeleccionarSegundoUser = new JLabel("Seleccionar Usuario");
		
		JComboBox comboBoxSegundoUsr = new JComboBox();
		
		JButton btnSeleccionarSegundoUsr = new JButton("Seleccionar");
		
		JLabel lblListaDelUsr = new JLabel("Listas del Usuario");
		
		JComboBox comboBox_1 = new JComboBox();
		
		JButton btnSeleccionarLista = new JButton("Seleccionar");
		
		JButton btnCancelar = new JButton("Cancelar");
		GroupLayout groupLayout_1 = new GroupLayout(internalFrameListasDeUsuario.getContentPane());
		groupLayout_1.setHorizontalGroup(
			groupLayout_1.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout_1.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout_1.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout_1.createParallelGroup(Alignment.TRAILING, false)
							.addComponent(comboBox_1, Alignment.LEADING, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(comboBoxSegundoUsr, Alignment.LEADING, 0, 505, Short.MAX_VALUE)
							.addComponent(lblSeleccionarSegundoUser, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 161, GroupLayout.PREFERRED_SIZE)
							.addComponent(btnSeleccionarSegundoUsr, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
							.addComponent(lblListaDelUsr, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE)
							.addComponent(btnCancelar))
						.addComponent(btnSeleccionarLista, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(38, Short.MAX_VALUE))
		);
		groupLayout_1.setVerticalGroup(
			groupLayout_1.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout_1.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblSeleccionarSegundoUser, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(comboBoxSegundoUsr, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnSeleccionarSegundoUsr)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblListaDelUsr)
					.addGap(18)
					.addComponent(comboBox_1, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnSeleccionarLista)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnCancelar)
					.addContainerGap(46, Short.MAX_VALUE))
		);
		internalFrameListasDeUsuario.getContentPane().setLayout(groupLayout_1);
		getContentPane().setLayout(groupLayout);

	}
}
