package presentacion;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.util.List;
import java.util.Vector;
import java.util.ArrayList;
import java.util.Enumeration;
import javax.swing.AbstractButton;


import logica.DtListaDeReproduccion;
import logica.DtParticular;
import logica.DtPorDefecto;
import logica.DtUsuario;
import logica.IControllerListaDeReproduccion;
import logica.IControllerUsuario;
import logica.Tipo;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import java.awt.event.ActionListener;

import java.awt.event.ActionEvent;

public class FormCrearListaDeReproduccion extends JInternalFrame {
	private IControllerUsuario iControllerUsuario;
	private JTextField textLista;
	private JComboBox<String> comboBoxTipos;
	private JComboBox<DtUsuario> comboBoxUsuario;
	private JButton btnCerrarTipo;
	private JButton btnSeleccionarTipo;
	private JPanel panelDefault;
	private JPanel panel;
    private JButton btnCancelarDefault;
    private JButton btnAceptarDefault;
    private ButtonGroup grupoPrivacidad;
    
	/**
	 * Create the frame.
	 */
	public FormCrearListaDeReproduccion(IControllerUsuario iControllerUsuario,IControllerListaDeReproduccion iControllerListaDeReproduccion) {
		//Propiedades del JInternalFrame
		setResizable(true);
        setIconifiable(true);
        setMaximizable(true);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setClosable(true);
        setTitle("Crear Lista De Reproduccion");
        setBounds(30, 30, 366, 121);
 
        JLabel lblSeleccionarTipo = new JLabel("Tipo de Lista De Reproduccion");
        
        comboBoxTipos = new JComboBox<String>();
        
        btnSeleccionarTipo = new JButton("Seleccionar");
        btnSeleccionarTipo.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        	int d = comboBoxTipos.getSelectedIndex();
        	  if(d==0) {
        	   comboBoxTipos.setEnabled(false);
        	   btnCerrarTipo.setVisible(false);
        	   btnSeleccionarTipo.setVisible(false);
        	   setBounds(30, 30, 353, 192);
        	   panel.setVisible(false);
        	   panelDefault.setVisible(true);
        	  }
        	  else {
        		   panelDefault.setVisible(true);
        		   comboBoxTipos.setEnabled(false);
        		   btnCerrarTipo.setVisible(false);
        		   btnSeleccionarTipo.setVisible(false);
        		   setBounds(30, 30, 353, 372);
        		   btnAceptarDefault.setVisible(false);
        		   btnCancelarDefault.setVisible(false);
        		   panel.setVisible(true);
        	  }
        	}
        });
        
        btnCerrarTipo = new JButton("Cerrar");
        btnCerrarTipo.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        	limpiarFormulario();
        	}
        });
        
         panelDefault = new JPanel();
        
         panel = new JPanel();
        GroupLayout groupLayout = new GroupLayout(getContentPane());
        groupLayout.setHorizontalGroup(
        	groupLayout.createParallelGroup(Alignment.LEADING)
        		.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
        			.addContainerGap()
        			.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
        				.addComponent(panel, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 332, Short.MAX_VALUE)
        				.addComponent(panelDefault, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 332, Short.MAX_VALUE)
        				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
        					.addComponent(btnSeleccionarTipo)
        					.addPreferredGap(ComponentPlacement.RELATED, 137, Short.MAX_VALUE)
        					.addComponent(btnCerrarTipo))
        				.addComponent(lblSeleccionarTipo, Alignment.LEADING)
        				.addComponent(comboBoxTipos, Alignment.LEADING, 0, 332, Short.MAX_VALUE))
        			.addContainerGap())
        );
        groupLayout.setVerticalGroup(
        	groupLayout.createParallelGroup(Alignment.LEADING)
        		.addGroup(groupLayout.createSequentialGroup()
        			.addContainerGap()
        			.addComponent(lblSeleccionarTipo)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(comboBoxTipos, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
        				.addComponent(btnSeleccionarTipo)
        				.addComponent(btnCerrarTipo))
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(panelDefault, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(panel, GroupLayout.PREFERRED_SIZE, 171, GroupLayout.PREFERRED_SIZE)
        			.addContainerGap(198, Short.MAX_VALUE))
        );
        
        JLabel lblNombreDelUsuario = new JLabel("Nombre del Usuario");
        
        comboBoxUsuario = new JComboBox<DtUsuario>();
        
        JButton btnSeleccionarUsuario = new JButton("Seleccionar");
        btnSeleccionarUsuario.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				iControllerUsuario.seleccionarUsuario(comboBoxUsuario.getSelectedItem().toString());
				comboBoxUsuario.setEnabled(false);
			}
		});
        
        JRadioButton rdbtnPrivada = new JRadioButton("Privada");
        
        JRadioButton rdbtnPublica = new JRadioButton("Publica");
        
        grupoPrivacidad = new ButtonGroup();
        grupoPrivacidad.add(rdbtnPrivada);
        grupoPrivacidad.add(rdbtnPublica);
        
        JButton btnAceptarParticular = new JButton("Aceptar");
        btnAceptarParticular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 DtParticular nuevaLista = new DtParticular(textLista.getText(), getTipoDeBoton(grupoPrivacidad));
				 iControllerUsuario.crearListaDeReproduccion(nuevaLista);
				 iControllerListaDeReproduccion.ingresarListaPorDefecto(nuevaLista);
				 limpiarFormulario();
			}
		});
        
        JButton btnCancelarParticular = new JButton("Cerrar");
        btnCancelarParticular.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        	limpiarFormulario();
        	}
        });
        GroupLayout gl_panel = new GroupLayout(panel);
        gl_panel.setHorizontalGroup(
        	gl_panel.createParallelGroup(Alignment.LEADING)
        		.addGroup(gl_panel.createSequentialGroup()
        			.addContainerGap()
        			.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
        				.addGroup(gl_panel.createSequentialGroup()
        					.addComponent(rdbtnPrivada)
        					.addGap(18)
        					.addComponent(rdbtnPublica))
        				.addComponent(lblNombreDelUsuario)
        				.addComponent(comboBoxUsuario, 0, 308, Short.MAX_VALUE)
        				.addComponent(btnSeleccionarUsuario)
        				.addGroup(gl_panel.createSequentialGroup()
        					.addComponent(btnAceptarParticular)
        					.addPreferredGap(ComponentPlacement.RELATED, 140, Short.MAX_VALUE)
        					.addComponent(btnCancelarParticular)))
        			.addContainerGap())
        );
        gl_panel.setVerticalGroup(
        	gl_panel.createParallelGroup(Alignment.LEADING)
        		.addGroup(gl_panel.createSequentialGroup()
        			.addGap(8)
        			.addComponent(lblNombreDelUsuario)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(comboBoxUsuario, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(btnSeleccionarUsuario)
        			.addGap(18)
        			.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
        				.addComponent(rdbtnPrivada)
        				.addComponent(rdbtnPublica))
        			.addPreferredGap(ComponentPlacement.UNRELATED)
        			.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
        				.addComponent(btnAceptarParticular)
        				.addComponent(btnCancelarParticular))
        			.addContainerGap(92, Short.MAX_VALUE))
        );
        panel.setLayout(gl_panel);
        
        JLabel lblNombreDeLa = new JLabel("Nombre de la Lista");
        
        textLista = new JTextField();
        textLista.setColumns(10);
        
        btnAceptarDefault = new JButton("Aceptar");
        btnAceptarDefault.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				   DtPorDefecto nuevaLista = new DtPorDefecto(textLista.getText(), Tipo.NINGUNO);
				   iControllerUsuario.crearListaDeReproduccion(nuevaLista);
				   limpiarFormulario();
			}
		});
        
        btnCancelarDefault = new JButton("Cerrar");
        btnCancelarDefault.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		limpiarFormulario();
        	}
        });
        GroupLayout gl_panelDefault = new GroupLayout(panelDefault);
        gl_panelDefault.setHorizontalGroup(
        	gl_panelDefault.createParallelGroup(Alignment.LEADING)
        		.addGroup(gl_panelDefault.createSequentialGroup()
        			.addContainerGap()
        			.addGroup(gl_panelDefault.createParallelGroup(Alignment.LEADING)
        				.addGroup(gl_panelDefault.createSequentialGroup()
        					.addComponent(lblNombreDeLa)
        					.addGap(1)
        					.addComponent(textLista, GroupLayout.DEFAULT_SIZE, 158, Short.MAX_VALUE))
        				.addGroup(gl_panelDefault.createSequentialGroup()
        					.addComponent(btnAceptarDefault)
        					.addPreferredGap(ComponentPlacement.RELATED, 86, Short.MAX_VALUE)
        					.addComponent(btnCancelarDefault)))
        			.addContainerGap())
        );
        gl_panelDefault.setVerticalGroup(
        	gl_panelDefault.createParallelGroup(Alignment.LEADING)
        		.addGroup(gl_panelDefault.createSequentialGroup()
        			.addContainerGap()
        			.addGroup(gl_panelDefault.createParallelGroup(Alignment.BASELINE)
        				.addComponent(lblNombreDeLa)
        				.addComponent(textLista, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addGroup(gl_panelDefault.createParallelGroup(Alignment.BASELINE)
        				.addComponent(btnAceptarDefault)
        				.addComponent(btnCancelarDefault))
        			.addContainerGap(56, Short.MAX_VALUE))
        );
        panelDefault.setLayout(gl_panelDefault);
        getContentPane().setLayout(groupLayout);
        panel.setVisible(false);
        panelDefault.setVisible(false);
	}
	
	public void cargarTipos() {
		String[] tipos = {"Por Defecto" , "Particular"};
		DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>(tipos);
		comboBoxTipos.setModel(model);
	}
	
	public void cargarUsuariosCombo(){
		DefaultComboBoxModel<DtUsuario> modeloUsuario;
		ArrayList<DtUsuario> usuariosCol = (ArrayList<DtUsuario>) iControllerUsuario.listarDtUsuarios();
		if(!usuariosCol.isEmpty()){
			Vector<DtUsuario> array = new Vector<DtUsuario>(usuariosCol);
			modeloUsuario = new DefaultComboBoxModel<DtUsuario>(array);
			comboBoxUsuario.setModel(modeloUsuario);
		}
		
	}
	   
	   public Tipo getTipoDeBoton(ButtonGroup buttonGroup) {
	        for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
	            AbstractButton button = buttons.nextElement();

	            if (button.isSelected() && button.getText()== "Privada") {
	                return Tipo.PRIVADO;
	            }
	            else return Tipo.PUBLICO;
	        }

	        return Tipo.NINGUNO;
	    }
	public void limpiarFormulario() {
		   setVisible(false);
		   btnSeleccionarTipo.setVisible(true);
		   btnCerrarTipo.setVisible(true);
		   btnAceptarDefault.setVisible(true);
		   btnCancelarDefault.setVisible(true);
		   panel.setVisible(false);
		   panelDefault.setVisible(false);
		   comboBoxTipos.setEnabled(true);
		   comboBoxTipos.setSelectedIndex(0);
		   comboBoxUsuario.setEnabled(true);
		   textLista.setText("");
		   setBounds(30,30,366,121);
	}
}
