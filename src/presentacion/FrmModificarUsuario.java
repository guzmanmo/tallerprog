package presentacion;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.Image;

import com.sun.prism.Graphics;
import com.sun.xml.internal.ws.org.objectweb.asm.Label;

import javafx.scene.control.RadioButton;
import logica.DtCanal;
import logica.DtListaDeReproduccion;
import logica.DtUsuario;
import logica.DtVideo;
import logica.IControllerUsuario;
import logica.Tipo;

import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Set;
import java.util.Vector;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JTextPane;
import javax.swing.JTextArea;
import javax.swing.JRadioButton;
import java.awt.Choice;
import java.awt.List;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class FrmModificarUsuario extends JInternalFrame {
	private JTextField txtNickName;
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtCorreo;
	private JTextField txtNombreCanal;
	private ButtonGroup radioGroup;
	private JRadioButton rdbtnPublico;
	private JRadioButton rdbtnPrivado;
	private JSpinner spinnerDia;
	private JSpinner spinnerMes;
	private JSpinner spinnerAnio;
	private JEditorPane txtDescripcionCanal;
	private Boolean checkImagenCambiada;
	private JComboBox<DtUsuario> comboBoxUsuario;
	private JComboBox<DtVideo> comboBoxVideo;
	private JComboBox<DtListaDeReproduccion> comboBoxLista;
	private IControllerUsuario iCtrlUsuario;
	private String nickAModificar;
	private String imagenUsuario;
	private JLabel lblImagenDePerfil;
	
	public FrmModificarUsuario(IControllerUsuario iCtrlUsuario) {
		this.iCtrlUsuario = iCtrlUsuario;
		cargarUsuariosCombo();
		
		setResizable(true);
		checkImagenCambiada = false;
		setTitle("Modificar Usuario");
		setBounds(100, 100, 805, 575);
		setLocation(0, 0);
		
		JPanel panelUsuarioCanal = new JPanel();
		
		JPanel panel = new JPanel();
		
		JLabel lblModificarVideo = new JLabel("Modificar Video");
		
		comboBoxVideo = new JComboBox<DtVideo>();
		
		JButton btnModificarVideo = new JButton("Modificar Video");
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblModificarVideo)
						.addComponent(comboBoxVideo, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(13, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
					.addContainerGap(107, Short.MAX_VALUE)
					.addComponent(btnModificarVideo)
					.addGap(105))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblModificarVideo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(comboBoxVideo, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnModificarVideo)
					.addContainerGap(22, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		
		JPanel panel_1 = new JPanel();
		
		JLabel lblModificarListaDeReproduccion = new JLabel("Modificar Lista De Reproduccion");
		
		comboBoxLista = new JComboBox<DtListaDeReproduccion>();
		
		JButton btnModificarLista = new JButton("Modificar Lista");
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addComponent(lblModificarListaDeReproduccion)
						.addComponent(comboBoxLista, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(13, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, gl_panel_1.createSequentialGroup()
					.addContainerGap(131, Short.MAX_VALUE)
					.addComponent(btnModificarLista)
					.addGap(107))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblModificarListaDeReproduccion)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(comboBoxLista, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnModificarLista)
					.addContainerGap(22, Short.MAX_VALUE))
		);
		panel_1.setLayout(gl_panel_1);
		
		JPanel panelImagen = new JPanel();
		
		lblImagenDePerfil = new JLabel("");
		lblImagenDePerfil.setHorizontalAlignment(SwingConstants.CENTER);
		
		JButton btnImagen = new JButton("Cambiar..");
		btnImagen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/* Programamos JFileChooser */

				
				JFileChooser jFile = new JFileChooser();
				FileNameExtensionFilter filtro = new FileNameExtensionFilter("JPG & PNG Images", "jpg","png");
				jFile.setFileFilter(filtro);
				int seleccion = jFile.showOpenDialog(btnImagen);
				if(seleccion==jFile.APPROVE_OPTION){
					File imagen = jFile.getSelectedFile();
					btnImagen.setText(imagen.getPath());;
					checkImagenCambiada = true;
				}
				
			}
		});
		GroupLayout gl_panelImagen = new GroupLayout(panelImagen);
		gl_panelImagen.setHorizontalGroup(
			gl_panelImagen.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelImagen.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblImagenDePerfil, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
					.addGap(120)
					.addComponent(btnImagen, GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_panelImagen.setVerticalGroup(
			gl_panelImagen.createParallelGroup(Alignment.LEADING)
				.addComponent(lblImagenDePerfil, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
				.addGroup(gl_panelImagen.createSequentialGroup()
					.addContainerGap()
					.addComponent(btnImagen))
		);
		panelImagen.setLayout(gl_panelImagen);
		
		JPanel panelSeleccionarUsuario = new JPanel();
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addComponent(panelSeleccionarUsuario, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(panelUsuarioCanal, GroupLayout.PREFERRED_SIZE, 378, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(panelImagen, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 355, GroupLayout.PREFERRED_SIZE)
						.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 355, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(44, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(panelSeleccionarUsuario, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(panelImagen, GroupLayout.PREFERRED_SIZE, 68, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(panelUsuarioCanal, GroupLayout.PREFERRED_SIZE, 462, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(panel, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		
		comboBoxUsuario = new JComboBox<DtUsuario>();
		comboBoxUsuario.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange()==ItemEvent.SELECTED){
					String[] nickNameCorreo = comboBoxUsuario.getSelectedItem().toString().split("-");
					nickAModificar = nickNameCorreo[0];
					iCtrlUsuario.seleccionarUsuario(nickAModificar);
					DtUsuario user = iCtrlUsuario.mostrarInfoUsuario();
					DtCanal canalUser = iCtrlUsuario.mostrarInfoCanal();
					cargarDatosDeUsuarioCanalEnForm(user, canalUser);
					
					
				}
			}
		});
		
		
		JLabel lblSeleccioneUnUsuario = new JLabel("Seleccione un usuario");
		GroupLayout gl_panelSeleccionarUsuario = new GroupLayout(panelSeleccionarUsuario);
		gl_panelSeleccionarUsuario.setHorizontalGroup(
			gl_panelSeleccionarUsuario.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelSeleccionarUsuario.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panelSeleccionarUsuario.createParallelGroup(Alignment.LEADING)
						.addComponent(lblSeleccioneUnUsuario)
						.addComponent(comboBoxUsuario, 0, 330, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_panelSeleccionarUsuario.setVerticalGroup(
			gl_panelSeleccionarUsuario.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelSeleccionarUsuario.createSequentialGroup()
					.addGap(5)
					.addComponent(lblSeleccioneUnUsuario)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(comboBoxUsuario, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panelSeleccionarUsuario.setLayout(gl_panelSeleccionarUsuario);
		
		JPanel panelGuardar = new JPanel();
		
		JButton btnGuardar = new JButton("Guardar");
		
		JButton btnCancelar = new JButton("Cancelar");
		GroupLayout gl_panelGuardar = new GroupLayout(panelGuardar);
		gl_panelGuardar.setHorizontalGroup(
			gl_panelGuardar.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelGuardar.createSequentialGroup()
					.addGap(81)
					.addComponent(btnGuardar, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnCancelar, GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE)
					.addGap(80))
		);
		gl_panelGuardar.setVerticalGroup(
			gl_panelGuardar.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelGuardar.createSequentialGroup()
					.addGap(5)
					.addGroup(gl_panelGuardar.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnGuardar)
						.addComponent(btnCancelar)))
		);
		panelGuardar.setLayout(gl_panelGuardar);
		
		JPanel panelFecha = new JPanel();
		
		JLabel lblFechaDeNacimiento = new JLabel("Fecha de Nacimiento");
		lblFechaDeNacimiento.setHorizontalAlignment(SwingConstants.CENTER);
		
		JPanel panelSpinner = new JPanel();
		GroupLayout gl_panelFecha = new GroupLayout(panelFecha);
		gl_panelFecha.setHorizontalGroup(
			gl_panelFecha.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelFecha.createSequentialGroup()
					.addGap(12)
					.addComponent(lblFechaDeNacimiento, GroupLayout.DEFAULT_SIZE, 147, Short.MAX_VALUE)
					.addGap(5)
					.addComponent(panelSpinner, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_panelFecha.setVerticalGroup(
			gl_panelFecha.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelFecha.createSequentialGroup()
					.addGroup(gl_panelFecha.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelFecha.createSequentialGroup()
							.addGap(7)
							.addComponent(lblFechaDeNacimiento))
						.addComponent(panelSpinner, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(26, Short.MAX_VALUE))
		);
		
		spinnerDia = new JSpinner();
		panelSpinner.add(spinnerDia);
		spinnerDia.setModel(new SpinnerNumberModel(1, 1, 31, 1));
		
		spinnerMes = new JSpinner();
		panelSpinner.add(spinnerMes);
		spinnerMes.setModel(new SpinnerNumberModel(1, 1, 12, 1));
		
		spinnerAnio = new JSpinner();
		panelSpinner.add(spinnerAnio);
		spinnerAnio.setModel(new SpinnerNumberModel(new Integer(1970), new Integer(1970), null, new Integer(1)));
		panelFecha.setLayout(gl_panelFecha);
		
		JPanel panelCorreo = new JPanel();
		
		JLabel lblCorreo = new JLabel("Correo");
		lblCorreo.setHorizontalAlignment(SwingConstants.CENTER);
		
		txtCorreo = new JTextField();
		txtCorreo.setHorizontalAlignment(SwingConstants.CENTER);
		txtCorreo.setColumns(10);
		GroupLayout gl_panelCorreo = new GroupLayout(panelCorreo);
		gl_panelCorreo.setHorizontalGroup(
			gl_panelCorreo.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelCorreo.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblCorreo, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGap(169)
					.addComponent(txtCorreo, GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_panelCorreo.setVerticalGroup(
			gl_panelCorreo.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelCorreo.createSequentialGroup()
					.addGap(7)
					.addGroup(gl_panelCorreo.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtCorreo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblCorreo)))
		);
		panelCorreo.setLayout(gl_panelCorreo);
		
		JPanel panelApellido = new JPanel();
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setHorizontalAlignment(SwingConstants.CENTER);
		
		txtApellido = new JTextField();
		txtApellido.setHorizontalAlignment(SwingConstants.CENTER);
		txtApellido.setColumns(10);
		GroupLayout gl_panelApellido = new GroupLayout(panelApellido);
		gl_panelApellido.setHorizontalGroup(
			gl_panelApellido.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelApellido.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblApellido, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGap(160)
					.addComponent(txtApellido, GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_panelApellido.setVerticalGroup(
			gl_panelApellido.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelApellido.createSequentialGroup()
					.addGap(7)
					.addGroup(gl_panelApellido.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtApellido, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblApellido)))
		);
		panelApellido.setLayout(gl_panelApellido);
		
		JPanel panelNombre = new JPanel();
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setHorizontalAlignment(SwingConstants.CENTER);
		
		txtNombre = new JTextField();
		txtNombre.setHorizontalAlignment(SwingConstants.CENTER);
		txtNombre.setColumns(10);
		GroupLayout gl_panelNombre = new GroupLayout(panelNombre);
		gl_panelNombre.setHorizontalGroup(
			gl_panelNombre.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelNombre.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNombre, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGap(162)
					.addComponent(txtNombre, GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_panelNombre.setVerticalGroup(
			gl_panelNombre.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelNombre.createSequentialGroup()
					.addGap(7)
					.addGroup(gl_panelNombre.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNombre)
						.addComponent(txtNombre, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
		);
		panelNombre.setLayout(gl_panelNombre);
		
		JPanel panelNick = new JPanel();
		
		JLabel lblNickname = new JLabel("NickName");
		lblNickname.setHorizontalAlignment(SwingConstants.CENTER);
		
		txtNickName = new JTextField();
		txtNickName.setHorizontalAlignment(SwingConstants.CENTER);
		txtNickName.setColumns(10);
		GroupLayout gl_panelNick = new GroupLayout(panelNick);
		gl_panelNick.setHorizontalGroup(
			gl_panelNick.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelNick.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNickname, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGap(148)
					.addComponent(txtNickName, GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_panelNick.setVerticalGroup(
			gl_panelNick.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelNick.createSequentialGroup()
					.addGap(5)
					.addGroup(gl_panelNick.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNickname)
						.addComponent(txtNickName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
		);
		panelNick.setLayout(gl_panelNick);
		
		JPanel panelNombreCanal = new JPanel();
		
		JPanel panelDescripcion = new JPanel();
		
		JPanel panelTipo = new JPanel();
		GroupLayout gl_panelUsuarioCanal = new GroupLayout(panelUsuarioCanal);
		gl_panelUsuarioCanal.setHorizontalGroup(
			gl_panelUsuarioCanal.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelUsuarioCanal.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panelUsuarioCanal.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelUsuarioCanal.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panelUsuarioCanal.createParallelGroup(Alignment.TRAILING)
								.addGroup(Alignment.LEADING, gl_panelUsuarioCanal.createSequentialGroup()
									.addGroup(gl_panelUsuarioCanal.createParallelGroup(Alignment.TRAILING)
										.addComponent(panelNick, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(panelNombre, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(panelApellido, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(panelCorreo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addContainerGap())
								.addGroup(Alignment.LEADING, gl_panelUsuarioCanal.createSequentialGroup()
									.addComponent(panelFecha, GroupLayout.PREFERRED_SIZE, 356, Short.MAX_VALUE)
									.addGap(11))
								.addGroup(Alignment.LEADING, gl_panelUsuarioCanal.createSequentialGroup()
									.addComponent(panelNombreCanal, GroupLayout.PREFERRED_SIZE, 355, GroupLayout.PREFERRED_SIZE)
									.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.addGroup(Alignment.LEADING, gl_panelUsuarioCanal.createSequentialGroup()
									.addComponent(panelGuardar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
							.addGroup(Alignment.TRAILING, gl_panelUsuarioCanal.createSequentialGroup()
								.addComponent(panelDescripcion, GroupLayout.PREFERRED_SIZE, 355, GroupLayout.PREFERRED_SIZE)
								.addContainerGap()))
						.addGroup(Alignment.TRAILING, gl_panelUsuarioCanal.createSequentialGroup()
							.addComponent(panelTipo, GroupLayout.PREFERRED_SIZE, 355, GroupLayout.PREFERRED_SIZE)
							.addContainerGap())))
		);
		gl_panelUsuarioCanal.setVerticalGroup(
			gl_panelUsuarioCanal.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelUsuarioCanal.createSequentialGroup()
					.addContainerGap()
					.addComponent(panelNick, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panelNombre, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panelApellido, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panelCorreo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(panelFecha, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(panelNombreCanal, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(panelTipo, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
					.addGap(31)
					.addComponent(panelDescripcion, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGap(18)
					.addComponent(panelGuardar, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
					.addGap(23))
		);
		
		JLabel lblPrivacidadCanal = new JLabel("Privacidad Canal");
		radioGroup = new ButtonGroup();
		rdbtnPublico = new JRadioButton("Publico");
		rdbtnPrivado = new JRadioButton("Privado");
		radioGroup.add(rdbtnPrivado);
		radioGroup.add(rdbtnPublico);
		
		GroupLayout gl_panelTipo = new GroupLayout(panelTipo);
		gl_panelTipo.setHorizontalGroup(
			gl_panelTipo.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelTipo.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblPrivacidadCanal)
					.addPreferredGap(ComponentPlacement.RELATED, 67, Short.MAX_VALUE)
					.addComponent(rdbtnPublico, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
					.addGap(5)
					.addComponent(rdbtnPrivado, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE))
		);
		gl_panelTipo.setVerticalGroup(
			gl_panelTipo.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelTipo.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panelTipo.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panelTipo.createParallelGroup(Alignment.LEADING)
							.addComponent(rdbtnPublico)
							.addComponent(rdbtnPrivado))
						.addComponent(lblPrivacidadCanal))
					.addContainerGap(6, Short.MAX_VALUE))
		);
		panelTipo.setLayout(gl_panelTipo);
		
		JLabel lblDescripcion = new JLabel("Descripcion de Canal");
		
		txtDescripcionCanal = new JTextPane();
		GroupLayout gl_panelDescripcion = new GroupLayout(panelDescripcion);
		gl_panelDescripcion.setHorizontalGroup(
			gl_panelDescripcion.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelDescripcion.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panelDescripcion.createParallelGroup(Alignment.LEADING)
						.addComponent(lblDescripcion)
						.addComponent(txtDescripcionCanal, GroupLayout.PREFERRED_SIZE, 332, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(11, Short.MAX_VALUE))
		);
		gl_panelDescripcion.setVerticalGroup(
			gl_panelDescripcion.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelDescripcion.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblDescripcion)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(txtDescripcionCanal, GroupLayout.PREFERRED_SIZE, 62, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panelDescripcion.setLayout(gl_panelDescripcion);
		
		JLabel lblNombreCanal = new JLabel("Nombre Canal");
		
		txtNombreCanal = new JTextField();
		txtNombreCanal.setColumns(10);
		GroupLayout gl_panelNombreCanal = new GroupLayout(panelNombreCanal);
		gl_panelNombreCanal.setHorizontalGroup(
			gl_panelNombreCanal.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelNombreCanal.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNombreCanal)
					.addPreferredGap(ComponentPlacement.RELATED, 118, Short.MAX_VALUE)
					.addComponent(txtNombreCanal, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_panelNombreCanal.setVerticalGroup(
			gl_panelNombreCanal.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panelNombreCanal.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGroup(gl_panelNombreCanal.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNombreCanal)
						.addComponent(txtNombreCanal, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		panelNombreCanal.setLayout(gl_panelNombreCanal);
		panelUsuarioCanal.setLayout(gl_panelUsuarioCanal);
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				limpiarDatos();
				setVisible(false);
			}
		

		});
		
		
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(checkForm()){
					try {
						
						/*MANEJO DE LA IMAGEN */ 
						String imgUsuario = "";
						if(checkImagenCambiada){
							Path origen = Paths.get(btnImagen.getText());
							String rutaProyecto = System.getProperty("user.dir");
							rutaProyecto = rutaProyecto+"/img/";
							int cantImagenesEnDirectorio = new File(rutaProyecto).listFiles().length;
							imgUsuario = rutaProyecto + cantImagenesEnDirectorio;
							Path rutaDestinoYNombre = Paths.get(imgUsuario);
							Files.copy(origen,rutaDestinoYNombre,StandardCopyOption.REPLACE_EXISTING);
						}
						/* Manejo Dt's */
						int anio = (int)spinnerAnio.getValue();
						int mes = (int)spinnerMes.getValue();
						int dia = (int)spinnerDia.getValue();
						Date fechaNacimiento = new Date(anio, mes, dia);
						DtUsuario dtUser = new DtUsuario(imgUsuario, fechaNacimiento, txtApellido.getText(),txtNombre.getText() , txtCorreo.getText(), txtNickName.getText());
						Tipo tipo = Tipo.PRIVADO;
						if(rdbtnPublico.isSelected()){
							tipo = Tipo.PUBLICO;
						}
						DtCanal dtCanal = new DtCanal(txtNombreCanal.getText(), txtDescripcionCanal.getText(), tipo);
						
						/* Operaciones con iControllerUsuario */
						iCtrlUsuario.seleccionarUsuario(nickAModificar);
						iCtrlUsuario.modificarUsuario(dtUser);
						iCtrlUsuario.modificarCanal(dtCanal);
					} catch (IOException e1) { }
					setVisible(false);
					limpiarDatos();
				}
			}
		});
		getContentPane().setLayout(groupLayout);
	}
	
	

	
	private void limpiarDatos() {
		txtNickName.setText("");
		txtApellido.setText("");
		txtCorreo.setText("");
		txtNombre.setText("");
		txtDescripcionCanal.setText("");
		txtNombreCanal.setText("");
		spinnerDia.setValue(1);
		spinnerMes.setValue(1);
		spinnerAnio.setValue(1970);
		rdbtnPrivado.setSelected(false);
		rdbtnPublico.setSelected(false);
	}

	public void cargarUsuariosCombo(){
		DefaultComboBoxModel<DtUsuario> modeloUsuario;
		ArrayList<DtUsuario> usuariosCol = (ArrayList<DtUsuario>) iCtrlUsuario.listarDtUsuarios();
		if(!usuariosCol.isEmpty()){
			Vector<DtUsuario> array = new Vector<DtUsuario>(usuariosCol);
			modeloUsuario = new DefaultComboBoxModel<DtUsuario>(array);
			comboBoxUsuario.setModel(modeloUsuario);
		}
		
	}
	

	public void cargarDatosDeUsuarioCanalEnForm(DtUsuario user,DtCanal canal){
		txtApellido.setText(user.getApellido());
		txtCorreo.setText(user.getEmail());
		txtDescripcionCanal.setText(canal.getDescripcion());
		txtNickName.setText(user.getNickName());
		txtNombre.setText(user.getNombre());
		txtNombreCanal.setText(canal.getNombre());
		spinnerDia.setValue(user.getFechaNacimiento().getDay());
		spinnerMes.setValue(user.getFechaNacimiento().getMonth());
		spinnerAnio.setValue(user.getFechaNacimiento().getYear());

		if(canal.getTipo().equals(Tipo.PRIVADO)){
			rdbtnPrivado.setSelected(true);
		}
		else{
			rdbtnPublico.setSelected(true);
		}
		
		imagenUsuario= user.getImagen();
		System.out.println(imagenUsuario);
		/*ImageIcon imgPerfilAux = new ImageIcon(getClass().getResource(imagenUsuario));
		ImageIcon img = new ImageIcon(imgPerfilAux.getImage().getScaledInstance(lblImagenDePerfil.getWidth(), lblImagenDePerfil.getHeight(), Image.SCALE_DEFAULT));
		lblImagenDePerfil.setIcon(img);*/
		
		
	}
	
	
	public boolean checkForm(){
		String nickName = txtNickName.getText();
		String correo = txtCorreo.getText();
		Boolean radioBtnIsSelected = rdbtnPrivado.isSelected() || rdbtnPublico.isSelected();
		if(nickName.isEmpty() || correo.isEmpty() || !radioBtnIsSelected){
			JOptionPane.showMessageDialog(this, "No puede haber campos vacios","Agregar Usuario", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		else{
			return true;

		}
		
	}
}

