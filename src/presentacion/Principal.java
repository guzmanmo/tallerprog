package presentacion;

import java.awt.EventQueue;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import logica.Factory;

import logica.IControllerCategoria;
import logica.IControllerListaDeReproduccion;
import logica.IControllerUsuario;
import sun.reflect.generics.tree.Tree;

import java.awt.Toolkit;

import java.awt.Dimension;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Principal extends JFrame {

	private JPanel desktop;
	private IControllerCategoria ICtrlCategoria;
	private IControllerUsuario iCtrlUsuario;
	private IControllerListaDeReproduccion iCtrlListaDeReproduccion;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal frame = new Principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Principal() {
		Factory fabrica = Factory.getInstancia();
		ICtrlCategoria =fabrica.getICategoria();
		iCtrlUsuario = fabrica.getIUsuario();
		iCtrlListaDeReproduccion = fabrica.getIListaDeReproduccion();
		desktop = new JPanel();
		desktop.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(desktop);
		desktop.setLayout(null);
		
		/* */
		
		
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension dimensionScreen = toolkit.getScreenSize();
		int anchoScreen = dimensionScreen.width;
		int altoScreen = dimensionScreen.height;
		/*Usuario */

		//FrmAgregarUsuario ventanaAgregarUsuario = new FrmAgregarUsuario(anchoScreen,altoScreen);
		FrmAgregarUsuario ventanaAgregarUsuario = new FrmAgregarUsuario(iCtrlUsuario);
		desktop.add(ventanaAgregarUsuario);
		ventanaAgregarUsuario.setVisible(false);
		
		/* Modificar Usuario */
		
		FrmModificarUsuario ventanaModificarUsuario = new FrmModificarUsuario(iCtrlUsuario);
		desktop.add(ventanaModificarUsuario);
		ventanaModificarUsuario.setVisible(false);
		
		/*Categoria */
		
		FormAltaCategoria ventanaAltaCategoria = new FormAltaCategoria();
		desktop.add(ventanaAltaCategoria);
		ventanaAltaCategoria.setVisible(false);
		
		/*Lista De Reproduccion*/
		
		FormCrearListaDeReproduccion ventanaCrearLista = new FormCrearListaDeReproduccion(iCtrlUsuario,iCtrlListaDeReproduccion);
		desktop.add(ventanaCrearLista);
		ventanaCrearLista.setVisible(false);
		
		
		FormConsultaCategoria ventanaConsultaCategoria = new FormConsultaCategoria(ICtrlCategoria);
		desktop.add(ventanaConsultaCategoria);
		ventanaConsultaCategoria.cargarCategorias();
		ventanaConsultaCategoria.setVisible(false);
		
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, anchoScreen, altoScreen);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		/* MENU USUARIO */

		JMenu mnUsuario = new JMenu("Usuario");
		menuBar.add(mnUsuario);
		
		
		/* Sub MENU USUARIO */

		JMenuItem mntmAgregar = new JMenuItem("Agregar");
		mntmAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventanaAgregarUsuario.setVisible(true);
			}
		});
		mnUsuario.add(mntmAgregar);
		
		JMenuItem mntmModificar = new JMenuItem("Modificar");
		mntmModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventanaModificarUsuario.setVisible(true);
				ventanaModificarUsuario.cargarUsuariosCombo();
			}
		});
		mnUsuario.add(mntmModificar);
		
		
		/* MENU Categoria */

		
		JMenu mnCategoria = new JMenu("Categoria");
		menuBar.add(mnCategoria);
		
		
		/* Sub MENU Categoria */

		JMenuItem mntmAltaCategoria = new JMenuItem("Agregar");
		mntmAltaCategoria.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventanaAltaCategoria.setVisible(true);
			}
		});
		mnCategoria.add(mntmAltaCategoria);
		
		JMenuItem mntmConsultaCategoria= new JMenuItem("Consultar");
		
		mntmConsultaCategoria.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				ventanaConsultaCategoria.cargarCategorias();
				ventanaConsultaCategoria.setVisible(true);
			}
		});
		mnCategoria.add(mntmConsultaCategoria);
		
		
		/*Menu Lista */
		
		JMenu mnLista = new JMenu("Lista De Reproduccion");
		menuBar.add(mnLista);
		
		/*Sub MENU LISTA*/
		
		JMenuItem mntmCrearListaDeReproduccion = new JMenuItem("Crear Lista");
		mntmCrearListaDeReproduccion.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ventanaCrearLista.cargarTipos();
				ventanaCrearLista.setVisible(true);
				
			}
		});
		mnLista.add(mntmCrearListaDeReproduccion);
				
		
		

	}
}
