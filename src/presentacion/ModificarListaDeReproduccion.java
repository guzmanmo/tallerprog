package presentacion;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JRadioButton;

public class ModificarListaDeReproduccion extends JInternalFrame {
	

	/**
	 * Create the frame.
	 */
	public ModificarListaDeReproduccion() {
		setBounds(100, 100, 621, 494);
		
		JLabel lblSeleccionarUsuario = new JLabel("Seleccionar Usuario");
		
		JComboBox comboBoxUsuario = new JComboBox();
		
		JButton btnSeleccionar = new JButton("Seleccionar");
		
		JInternalFrame iFrameListaParticular = new JInternalFrame("Listas Particulares de Usuario");
		iFrameListaParticular.setVisible(true);
		
		JInternalFrame iFrameCategoriaLista = new JInternalFrame("Categoria y Privacidad de la Lista");
		iFrameCategoriaLista.setVisible(true);
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblSeleccionarUsuario, GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
							.addGap(326))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnSeleccionar)
							.addContainerGap(610, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(comboBoxUsuario, GroupLayout.PREFERRED_SIZE, 419, GroupLayout.PREFERRED_SIZE)
							.addContainerGap())
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(iFrameListaParticular, Alignment.LEADING, 0, 0, Short.MAX_VALUE)
								.addComponent(iFrameCategoriaLista, Alignment.LEADING))
							.addContainerGap(204, Short.MAX_VALUE))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblSeleccionarUsuario)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(comboBoxUsuario, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnSeleccionar)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(iFrameListaParticular, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(iFrameCategoriaLista, GroupLayout.PREFERRED_SIZE, 171, GroupLayout.PREFERRED_SIZE)
					.addGap(145))
		);
		
		JLabel lblCategoriaDeLaLista = new JLabel("Categoria de la Lista");
		
		JComboBox comboBoxCategoria = new JComboBox();
		
		JRadioButton rdbtnPrivada = new JRadioButton("Privada");
		
		JRadioButton rdbtnPublica = new JRadioButton("Publica");
		
		ButtonGroup grupoPrivacidad = new ButtonGroup();
		grupoPrivacidad.add(rdbtnPublica);
		grupoPrivacidad.add(rdbtnPrivada);
		
		GroupLayout groupLayout_2 = new GroupLayout(iFrameCategoriaLista.getContentPane());
		groupLayout_2.setHorizontalGroup(
			groupLayout_2.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout_2.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout_2.createParallelGroup(Alignment.LEADING)
						.addComponent(comboBoxCategoria, GroupLayout.PREFERRED_SIZE, 399, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblCategoriaDeLaLista)
						.addGroup(groupLayout_2.createSequentialGroup()
							.addComponent(rdbtnPrivada)
							.addGap(34)
							.addComponent(rdbtnPublica)))
					.addContainerGap(101, Short.MAX_VALUE))
		);
		groupLayout_2.setVerticalGroup(
			groupLayout_2.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout_2.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblCategoriaDeLaLista)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(comboBoxCategoria, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(groupLayout_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(rdbtnPrivada)
						.addComponent(rdbtnPublica))
					.addContainerGap(27, Short.MAX_VALUE))
		);
		iFrameCategoriaLista.getContentPane().setLayout(groupLayout_2);
		
		JLabel lblListasDeReproduccion = new JLabel("Listas de Reproduccion Particulares del Usuario");
		
		JComboBox comboBoxLista = new JComboBox();
		
		JButton btnSeleccionarCaterogira = new JButton("Seleccionar");
		GroupLayout groupLayout_1 = new GroupLayout(iFrameListaParticular.getContentPane());
		groupLayout_1.setHorizontalGroup(
			groupLayout_1.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout_1.createSequentialGroup()
					.addGroup(groupLayout_1.createParallelGroup(Alignment.LEADING)
						.addComponent(lblListasDeReproduccion)
						.addGroup(groupLayout_1.createSequentialGroup()
							.addGap(12)
							.addComponent(comboBoxLista, GroupLayout.PREFERRED_SIZE, 397, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout_1.createSequentialGroup()
							.addContainerGap()
							.addComponent(btnSeleccionarCaterogira)))
					.addContainerGap(113, Short.MAX_VALUE))
		);
		groupLayout_1.setVerticalGroup(
			groupLayout_1.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout_1.createSequentialGroup()
					.addComponent(lblListasDeReproduccion)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(comboBoxLista, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnSeleccionarCaterogira)
					.addContainerGap(205, Short.MAX_VALUE))
		);
		iFrameListaParticular.getContentPane().setLayout(groupLayout_1);
		getContentPane().setLayout(groupLayout);

	}
}
