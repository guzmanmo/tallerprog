package excepciones;

public class NoExisteElementoException extends Exception {
	   
	public NoExisteElementoException(String name) {
		   super(name);
	}
}
