package logica;

public class Valoracion {
	private EValoracion valoracion;
	
	public Valoracion(EValoracion val) {
		this.valoracion=val;
	}
	
	public EValoracion getValoracion() {
		return this.valoracion;
	}
	
	public void setValoracion(EValoracion val) {
		this.valoracion=val;
	}
}
