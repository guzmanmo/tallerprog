package logica;
import java.util.Set;

public interface IControllerListaDeReproduccion {
	
	   public abstract Boolean seleccionarListaDeReproduccion (String name);
	   
	   public abstract DtListaDeReproduccion mostrarInfoListaDeReproduccion();
	   
	   public abstract Set<DtVideo> listarDtVideoEnUnaListaDeReproduccion();
	   
	   public abstract void ingresarListaPorDefecto(DtListaDeReproduccion lista);
	   
}
