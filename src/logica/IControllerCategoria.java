package logica;

import java.util.Set;

public interface IControllerCategoria {
	public abstract void crearCategoria(DtCategoria category);

	public abstract Set<DtCategoria> listarDtCategoria();

	public abstract void seleccionarCategoria(String nombreCategoria);

	public abstract void agregarCategoria(String nombreCategoria);

	public abstract Categoria getCategoriaRecordada();

}
