package logica;

import java.util.List;

public interface IControllerUsuario {
	public abstract void seleccionarUsuario(String nickName);
	public abstract void BorrarMemoria();
	public abstract void modificarUsuario(DtUsuario user);
	public abstract List<DtUsuario> listarDtUsuarios();
	public abstract DtUsuario mostrarInfoUsuario();
	//public abstract List<DtVideo> listarDtVideo();
	public abstract DtCanal mostrarInfoCanal();
	public abstract void insertarDataUser(DtUsuario dataUser);
	public abstract void insertarDataCanal(DtCanal canal);
	public abstract void darAltaUsuario();
	public abstract void seguirAUsuario(String seguidor, String seguido);
	public abstract void DejarSeguirAUsuario(String seguidor, String seguido);
	public abstract void crearListaDeReproduccion(DtListaDeReproduccion lista);
	public abstract void modificarCanal(DtCanal canal);
	
	

}
