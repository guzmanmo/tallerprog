package logica;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 * @author tpgr26 
 *
 *	Redefine el map para doble clave
 */
public class ManejadorUsuario{
	private Map<String, Usuario> usuariosEmail;
	private Map<String, Usuario> usuariosNickName;
	private static ManejadorUsuario instancia = null;
	private Usuario uRecordado;
	 
	
	private ManejadorUsuario(){
		usuariosEmail = new HashMap<String, Usuario>();
		usuariosNickName = new HashMap<String, Usuario>();
	}
	
	
	public static ManejadorUsuario getInstancia() {
		   if(instancia == null ) {
			   instancia = new ManejadorUsuario();
		   }
		   return instancia;
	 }
	
	
	
	
	/**
     * Busca y recuerda la instancia del usuario nickname en el sistema
     */
	public void seleccionarUsuario(String nickName){
		uRecordado = findByNickname(nickName);
		
	}
	public void BorrarMemoria(){
		uRecordado = null;
	}
	
	
	public Usuario findByNickname(String nick){
		return usuariosNickName.get(nick);
	}
	public Usuario findByEmail(String email){
		return usuariosEmail.get(email);
	}
	public Usuario findByDt(DtUsuario dtU){
		if (dtU.getNickName().isEmpty()){
			return this.findByNickname(dtU.getNickName());
		}else{
			return this.findByEmail(dtU.getEmail());
		}
	} 
	public void agregarUsuario(Usuario U){
		//Falta verificaci�n
		usuariosEmail.put(U.getEmail(), U);
		usuariosNickName.put(U.getNickName(), U);
	}
	public void RemoveByDt(DtUsuario dtU){
		usuariosEmail.remove(dtU.getEmail());
		usuariosNickName.remove(dtU.getNickName());
	}
	public void RemoveByNick(String nickName){
		//busco el usuario y llamo a remove by email
		usuariosNickName.remove(nickName);
	}
	public void RemoveByEmail(String email){
		//busco el usuario y llamo remove by nick
		usuariosEmail.remove(email);
	}
	public Iterator<Map.Entry<String, Usuario>> iterator(){
		return usuariosEmail.entrySet().iterator();
	}
 


	public List<DtUsuario> listarDtUsuarios() {
		List<DtUsuario> coldtuser= new ArrayList<DtUsuario>();
		for(Map.Entry<String, Usuario> entry : usuariosNickName.entrySet()) {
			Usuario u=entry.getValue();
			DtUsuario dt= u.getDtUsuario();
			coldtuser.add(dt);
		}
		return coldtuser; 
		

	}

 
} 
 
