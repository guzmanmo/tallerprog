package logica;

public class DtListaDeReproduccion {
	   private String nombre;
	   private Tipo tipo;
	  
	   public DtListaDeReproduccion(String nombre , Tipo tipo) {
		   this.nombre=nombre;
		   this.tipo=tipo;
	   }
	   public String getNombre() {
		   return this.nombre;
	   }
	   public Tipo getTipo() {
		   return this.tipo;
	   } 
	   
	   
}
