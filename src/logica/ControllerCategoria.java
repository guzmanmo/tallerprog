package logica;

import java.util.*;


public class ControllerCategoria implements IControllerCategoria {
	private Categoria categoriaRecordada;
	private Set<Categoria> categorias ;
	
	public  ControllerCategoria() {
		this.categoriaRecordada=null;
	};
	
	
	
	public void crearCategoria(DtCategoria category ) {
		Categoria c= new Categoria(category.getNombre());
		categorias.add(c);
	}
	
	public Set<DtCategoria> listarDtCategoria(){
		ManejadorCategoria mc = ManejadorCategoria.getInstancia();
		return mc.listarDtCategoria();
	}
	
	public void seleccionarCategoria(String nombreCategoria) {
		ManejadorCategoria mc = ManejadorCategoria.getInstancia();
		this.categoriaRecordada=mc.seleccionarCategoria(nombreCategoria);
	}
	public void agregarCategoria(String nombreCategoria) {
		ManejadorCategoria mc = ManejadorCategoria.getInstancia();
		mc.agregarCategoria(nombreCategoria);

		
	}
	
	public Categoria getCategoriaRecordada() {
		return categoriaRecordada;
	}
	
}
