package logica;

import java.util.*;

public class Comentario {
	private String texto;
	private DtFecha fecha;
	private Set<Comentario> respuestas;
	
	public Comentario(String text,DtFecha date) {
		this.texto=text;
		this.fecha=date;
		this.respuestas=new HashSet<Comentario>();

	}
	
	public String getTexto() {
		return this.texto;
	}
	
	public DtFecha getFecha() {
		return this.fecha;
	}
	public void setTexto(String text) {
		this.texto=text;
	}
	public void setFecha(DtFecha date) {
		this.fecha=date;
	}
	
}
