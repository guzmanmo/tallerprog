package logica;

import java.util.*;

public class DtListaDeReproduccionYVideos {
	private DtListaDeReproduccion listaR;
	private Set<DtVideo> videosLista;
	
	public DtListaDeReproduccionYVideos(DtListaDeReproduccion lista ,Set<DtVideo> videos) {
		   this.listaR=lista;
		   videosLista=videos;
	}
	public DtListaDeReproduccion getDtListaDeReproduccion() {
		   return listaR;
		   
	}
	public Set<DtVideo> getSetDtVideo(){
		return videosLista;
	}
}
