package logica;

public class DtListaVideoPropietario {
	private String propietario;
	private String listaVideoName;
	
	
	public DtListaVideoPropietario(String p, String lvn) {
		// TODO Auto-generated constructor stub
		propietario=p;
		listaVideoName=lvn;
		
	}
	public String getListaVideoName() {
		return listaVideoName;
	}
	public String getPropietario() {
		return propietario;
	}
	public void setListaVideoName(String listaVideoName) {
		this.listaVideoName = listaVideoName;
	}
	public void setPropietario(String propietario) {
		this.propietario = propietario;
	}
	
	public String toString() {
        return getPropietario() +" - "+ getListaVideoName() ;
    }

}
