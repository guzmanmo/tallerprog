package logica;

import java.util.*;

public class ControllerListaDeReproduccion implements IControllerListaDeReproduccion {
	
	   private static ControllerListaDeReproduccion instancia=null;
	   private ListaDeReproduccion rLista;
	   
	   public ControllerListaDeReproduccion() {
		   rLista=null;
	   }
	   
	   
	   public ListaDeReproduccion getLista() {
		      return this.rLista;
	   }
	   public void setLista(ListaDeReproduccion ListaSeleccionada) {
		      this.rLista = ListaSeleccionada;
	   }
	   
	   public Boolean seleccionarListaDeReproduccion (String name) {
		     ManejadorListaDeReproduccion manejadorLista= ManejadorListaDeReproduccion.getInstancia();
		     this.rLista=manejadorLista.seleccionarListaDeReproduccion(name);
		     if(rLista == null) {
		    	 return false;
		     }else {
		    	 return true;
		     }
	   }
	   
	   public DtListaDeReproduccion mostrarInfoListaDeReproduccion(){
		      DtListaDeReproduccion infoLista = this.rLista.getDtListaDeReproduccion();
		      return infoLista;
	   } 
	   
	   public Set<DtVideo> listarDtVideoEnUnaListaDeReproduccion(){
		      Set<DtVideo> listaVideo = rLista.obtenerDtVideoDeLista();
		      return listaVideo;
	   }
	   public void ingresarListaPorDefecto(DtListaDeReproduccion lista) {
		     ManejadorListaDeReproduccion manejadorLista= ManejadorListaDeReproduccion.getInstancia();
		     manejadorLista.ingresarListaPorDefecto(lista);

		     
	   }
	   
	   public Set<DtListaDeReproduccion> listarSetDtListaDeReproduccion(){
		     ManejadorListaDeReproduccion manejadorLista= ManejadorListaDeReproduccion.getInstancia();
		     return manejadorLista.listarSetDtListaDeReproduccion();		     
	   }

 
}
	  