package logica;
import java.util.*;
import excepciones.NoExisteElementoException;

public class Canal {
	private String nombre;
	private String descripcion;
	private Tipo tipo;
	private Usuario usuario;
	private Set<ListaDeReproduccion> listasDeReproduccion;
	private Set<Video> videos;


	public Canal(String name, String desc, Tipo tipo){
		nombre = name;
		descripcion = desc;
		this.tipo = tipo;
		listasDeReproduccion = new HashSet<ListaDeReproduccion>();
		videos = new HashSet<Video>();
	}
	
	public String getNombre(){
		return nombre;
	}
	public String getDescripcion(){
		return descripcion;
	}
	public Tipo getTipo(){
		return tipo;
	}
	public void setNombre(String name){
		nombre = name;
	}
	public void setDescripcion(String desc){
		descripcion = desc;
	}
	public void setTipo(Tipo t){
		tipo = t;
	}
	public DtCanal getDtCanal(){
		return new DtCanal(nombre, descripcion, tipo);
	}
	public Set<DtVideo> obtenerDtVideoDeCanal(){
		Set<DtVideo> videosDt = new HashSet<DtVideo>();
		Iterator<Video> it = videos.iterator();
		while(it.hasNext()){
			Video video = it.next();
			DtVideo videoDt = video.getDtVideo();
			videosDt.add(videoDt);
		}
		return videosDt;
	}
	public Set<DtListaDeReproduccion> obtenerDtListaDeReproduccion(){
		Set<DtListaDeReproduccion> listasDt = new HashSet<DtListaDeReproduccion>();
		Iterator<ListaDeReproduccion> it = listasDeReproduccion.iterator();
		while(it.hasNext()){
			ListaDeReproduccion lista = it.next();
			DtListaDeReproduccion listaDt = lista.getDtListaDeReproduccion();
			listasDt.add(listaDt);
		}
		return listasDt;
	}
	public DtUsuario obtenerPropietarioCanal(){
		return usuario.getDtUsuario();
	}
	public void crearListaDeReproduccion(DtListaDeReproduccion listaDt){
		String nombreLista = listaDt.getNombre();
		Tipo tipoLista = listaDt.getTipo();
		if(listaDt instanceof DtParticular){
			Particular newLista = new Particular(nombreLista,tipoLista);
			listasDeReproduccion.add(newLista);

		}
		else{
			PorDefecto newLista = new PorDefecto(nombreLista,tipoLista);
			listasDeReproduccion.add(newLista);
		}
	}
	public void modificarListaDeReproduccion(String nombreL,DtListaDeReproduccion listaDt){
		Iterator<ListaDeReproduccion> it = listasDeReproduccion.iterator();
		while(it.hasNext()){
			ListaDeReproduccion actual = it.next();
			if(actual.getNombre().equals(nombreL)){
				String newNombre = listaDt.getNombre();
				Tipo newtipo = listaDt.getTipo();
				actual.setNombre(newNombre);
				switch(tipo){
				case PUBLICO:
					actual.setTipo(newtipo);
				case PRIVADO:
					Tipo t=Tipo.PRIVADO;
					actual.setTipo(t);
				}
			}
		}
	}
	
	public Set<DtVideo> darVideosEnListaDeReproduccion(DtListaDeReproduccion lista){
		String nombreL=lista.getNombre();
		Set<DtVideo> res=new HashSet<DtVideo>();
		Iterator<ListaDeReproduccion> it = listasDeReproduccion.iterator();
		while(it.hasNext()){
			ListaDeReproduccion actual = it.next();
			if(actual.getNombre().equals(nombreL)){
				res= actual.obtenerDtVideoDeLista();
			}
		}
		return res;
	}
	
	public DtListaDeReproduccionYVideos obtenerInfoListaDeReproduccion(String nombreL)throws NoExisteElementoException{
		DtListaDeReproduccionYVideos res=null;
		Iterator<ListaDeReproduccion> it = listasDeReproduccion.iterator();
		while(it.hasNext()){
			ListaDeReproduccion actual = it.next();
			if(actual.getNombre().equals(nombreL)){
				DtListaDeReproduccion listaRes = new DtListaDeReproduccion(actual.getNombre(),actual.getTipo());
				Set<DtVideo> videosDeLista = actual.obtenerDtVideoDeLista();
				res = new DtListaDeReproduccionYVideos(listaRes,videosDeLista);
				return res;
			}
			else
				throw new NoExisteElementoException(nombreL);
		}
		return res;
	}

	public List<DtVideo> listarDtVideo() {
		// TODO Auto-generated method stub
		return null;
	}
}