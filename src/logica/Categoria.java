package logica;

import java.util.*;


public class Categoria {
	private String nombre;
	private Set<ListaDeReproduccion> lista;
	private Set<Video> videos;
	private Set<Canal> canal;
	
	public Categoria(String name) {
		this.nombre=name;
		this.lista= new HashSet<ListaDeReproduccion>();
		this.videos= new HashSet<Video>();
		this.canal= new HashSet<Canal>();


	}
	
	public String getNombre() {
		return this.nombre;
		
	}
	public void setNombre(String name) {
		this.nombre=name;
		
	}
	
	public Set<DtVideo> obtenerDtVideo() {
		Set<DtVideo> dtVideo = new HashSet<DtVideo>();
		Iterator<Video> it= this.videos.iterator();
		while (it.hasNext()) {
			Video auxVideo = (Video) it.next();
			DtVideo dt = auxVideo.getDtVideo();
			dtVideo.add(dt);
		}
		return dtVideo;
	}
	
	public Set<DtListaDeReproduccion> obtenerListaDeReproduccion(){
		Set<DtListaDeReproduccion> dtLista= new HashSet<DtListaDeReproduccion>();
		Iterator<ListaDeReproduccion> it = this.lista.iterator();
		while (it.hasNext()) {
			ListaDeReproduccion auxLista= (ListaDeReproduccion) it.next();
			DtListaDeReproduccion dt = auxLista.getDtListaDeReproduccion();
			dtLista.add(dt);
		}
		return dtLista;
	}
	public DtCategoria getDtCategoria() {
		 DtCategoria dt = new DtCategoria(this.nombre);
		 return dt;	
	}

	public void agregarVideoCategoria(Video video) {
		videos.add(video);
		
	}
	
	public Set<DtListaVideoPropietario>  obtenerListaVideoPropietario() {
		Set<DtListaVideoPropietario> res = new HashSet<DtListaVideoPropietario>();
		Iterator<Video> it = videos.iterator();
		while (it.hasNext()) {
			Video video=it.next();
			DtUsuario dtUser= video.obtenerPropietarioVideo();
			DtListaVideoPropietario dt= new DtListaVideoPropietario(dtUser.getNickName(), video.getNombre());
			res.add(dt);
		}
		/* agregar Las Listas de reproduccion*/
		return res;
		
	}
	
}
