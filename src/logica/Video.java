package logica;

import java.util.*;


public class Video {
	private String nombre;
	private String descripcion;
	private Tipo tipo;
	private DtHora duracion;
	private Set<Comentario> comentarios;
	private Set<Valoracion> valoraciones;
	private Canal canal;
	private Categoria categoria;
	
	public Video(String name, String desc ,Tipo t,DtHora dur , String nombreCategoria){
		this.nombre=name;
		this.descripcion=desc;
		this.tipo=t;
		this.duracion = dur;
		this.comentarios=new HashSet<Comentario>();
		this.valoraciones= new HashSet<Valoracion>();
		ManejadorCategoria mc = ManejadorCategoria.getInstancia();
		this.categoria=mc.seleccionarCategoria(nombreCategoria);
		this.categoria.agregarVideoCategoria(this);
		

	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public Tipo getTipo() {
		return this.tipo;
	}
	
	public String getDescripcion() {
		return this.descripcion;
	}
	public DtHora getDuracion() {
		return duracion;
	}
	
	public void setNombre(String n) {
		this.nombre=n;
	}
	
	public void setDescripcion(String d) {
		this.descripcion=d;
	}

	
	public void setTipo(Tipo t) {
		this.tipo=t;
	}
	public void setDuracion(DtHora duracion) {
		this.duracion = duracion;
	}
	
	public DtVideo getDtVideo() {
		DtVideo dt= new DtVideo (this.nombre,this.descripcion,this.tipo);
		return dt ;
	}
	
	public DtUsuario obtenerPropietarioVideo() {
		return this.canal.obtenerPropietarioCanal();
	}
	
	
	
}
