package logica;

public class Factory {
	private static  Factory instancia=null;
	
	private Factory() {}
	
	public static Factory getInstancia(){
		if(instancia == null){
			instancia = new Factory();
			}
		return instancia;
	}
	public  IControllerCategoria getICategoria() {
		return  new ControllerCategoria();
		
	}

	public IControllerListaDeReproduccion getIListaDeReproduccion() {
		return new ControllerListaDeReproduccion();
	}
	public IControllerUsuario getIUsuario() {
		return new ControladorUsuario();
		
	}
}
