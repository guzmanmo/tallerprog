package logica;

public class DtHora {
	private int hora;
	private int minuto;
	
	
	public DtHora(int h, int m) {
		hora=h;
		minuto=m;
	}
	public int getHora() {
		return hora;
	}
	public int getMinuto() {
		return minuto;
	}
	public void setHora(int hora) {
		this.hora = hora;
	}
	public void setMinuto(int minuto) {
		this.minuto = minuto;
	}
	

}
