package logica;

import java.util.*;

public class ManejadorCategoria {
	private Set<Categoria> categorias;
	private static ManejadorCategoria instancia = null;
	
	private ManejadorCategoria() {
		categorias= new HashSet<Categoria>();
	}
	 public static  ManejadorCategoria getInstancia() {
		 if(instancia==null) {
			 return instancia= new ManejadorCategoria();
		}else {
				return instancia;
		}
	}
	 public Set<DtCategoria> listarDtCategoria(){
			Set<DtCategoria> dtCategoria= new HashSet<DtCategoria>();
			Iterator<Categoria> it= categorias.iterator();
			while(it.hasNext()) {
				Categoria auxCategoria=(Categoria) it.next();
				DtCategoria dt= auxCategoria.getDtCategoria();
				dtCategoria.add(dt);
			}
			return dtCategoria;
			
	 }
	 
	 public Categoria seleccionarCategoria(String nombreCategoria) {
			Iterator<Categoria> it= categorias.iterator();
			while(it.hasNext()) {
				Categoria category=(Categoria) it.next();
				if(category.getNombre()==nombreCategoria) {
					return category;

				}
				
			}
			return null;
		}
	 
	 public void agregarCategoria(String nombreCategoria) {
		 Categoria c = new Categoria(nombreCategoria);
		 categorias.add(c);
	 }
	 
	
	 
	 
	

}
