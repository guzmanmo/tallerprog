package logica;


public class DtCanal {
	private String nombre;
	private String descripcion;
	private Tipo tipo;
	
	
	public DtCanal(String name, String desc, Tipo tipo){
		nombre = name;
		descripcion = desc;
		this.tipo = tipo;
	}
	
	public String getNombre(){
		return nombre;
	}
	public String getDescripcion(){
		return descripcion;
	}
	public Tipo getTipo(){
		return tipo;
	}
	public void setNombre(String name){
		nombre = name;
	}
	public void setDescripcion(String desc){
		descripcion = desc;
	}
	public void setTipo(Tipo t){
		tipo = t;
	}

	public void modificarCanal(DtCanal canal) {
		nombre = canal.getNombre();
		descripcion = canal.getDescripcion();
		tipo=canal.getTipo();
		
	}
}
