package logica;

public class DtVideo {
	private String nombre;
	private String descripcion;
	private Tipo tipo;
	
	public DtVideo(String name, String desc ,Tipo t){
		this.nombre=name;
		this.descripcion=desc;
		this.tipo=t;
		
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public Tipo getTipo() {
		return this.tipo;
	}
	
	public String getDescripcion() {
		return this.descripcion;
	}
	
	public void setNombre(String n) {
		this.nombre=n;
	}
	
	public void setDescripcion(String d) {
		this.descripcion=d;
	}
	
	public void setTipo(Tipo t) {
		this.tipo=t;
	}
	
}
