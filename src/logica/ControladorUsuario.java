/**
 * 
 */
package logica;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sun.xml.internal.bind.v2.runtime.reflect.ListIterator;
/**
 * @author tpgr26 
 *
 */

public class ControladorUsuario implements IControllerUsuario{
/**	col usuarios**/
	private Usuario uRecordado;
	private DtUsuario dtU;
	private DtCanal dtC; 
	private ManejadorUsuario usuarios;
	private ManejadorUsuario mu;
	
	public ControladorUsuario() {
		mu = ManejadorUsuario.getInstancia();
	}
	
	/**
     * Busca y recuerda la instancia del usuario nickname en el sistema
     */
	public void seleccionarUsuario(String nickName){
		uRecordado = mu.findByNickname(nickName);
		
	}
	public void BorrarMemoria(){
		uRecordado = null;
	}
	
	public void modificarUsuario(DtUsuario user){
		uRecordado.modificarUsuario(user);
	}
	
	public void modificarCanal(DtCanal canal){
		uRecordado.modificarCanal(canal);
	}
	
	public List <DtUsuario> listarDtUsuarios(){
		return mu.listarDtUsuarios();
	}
	
	
	/**
     * Muestra la info del usuario recordado. Pre: Usuario ya recordado
     */
	public DtUsuario mostrarInfoUsuario(){
		return uRecordado.getDtUsuario();
	}
	
	/**
     * Retorna la coleccion de videos del usuario recordado. Pre:Usuario ya recordado.
     */
	public List <DtVideo> listarDtVideo(){
		return uRecordado.listarDtVideo();
	}
	/**
     * Retorna el canal del usuario recordado. Pre:Usuario ya recordado.
     */
	public DtCanal mostrarInfoCanal(){
		return uRecordado.getDtCanal();
	} 
	
	/**
     * Retorna las listas de reproduccion del usuario. Pre:Usuario ya recordado.
     */
	public Set<DtListaDeReproduccion> listarDtListaDeReproduccionUsuario(){
		return uRecordado.listarDtListaDeReproduccion();
	} 
	
	public List<logica.DtUsuario> listarSeguidoresDeUsuario(){
		return uRecordado.listarSeguidores();
	}
	
	public List<DtUsuario> listarSeguidosPorUsuario(){
		return uRecordado.listarSeguidos();
	}
	
	/**
     * Recuerda los datos
     */
	public void insertarDataUser(DtUsuario dataUser){
		dtU=dataUser;
	}
	
	/**
     * Recuerda los datos
     */
	public  void insertarDataCanal(DtCanal canal){
		dtC=canal;
	}
	
	/**
     * Toma los datos recordados y da de alta el usuario. Pre: DtUsuario y DtCanal recordado.
     */
	public void darAltaUsuario(){
		Usuario u= new Usuario(dtU,dtC);
		mu.agregarUsuario(u);
		
		/*
		 * Aqui debemos utilizar un controller, no el manejador.
		 * 
		ManejadorListaDeReproduccion clr= ManejadorListaDeReproduccion.getInstancia();
		Set<DtListaDeReproduccion> d=clr.listarSetDtListaDeReproduccion();*/
		
		ControllerListaDeReproduccion ctrlLista = new ControllerListaDeReproduccion();
		Set<DtListaDeReproduccion> d = ctrlLista.listarSetDtListaDeReproduccion();
		Iterator<DtListaDeReproduccion> it= d.iterator();
		while(it.hasNext()) {
			DtListaDeReproduccion dtl=(DtListaDeReproduccion) it.next();
			u.crearListaDeReproduccion(dtl); 
		} 
	}
	
	/**
     * Agrega un usuario seguido al usuario recordado. PRE: Usuario recordado
     */
	public void seguirAUsuario(String seguidor, String seguido){
		Usuario sigue=mu.findByNickname(seguidor);
		Usuario losiguen=mu.findByNickname(seguido);
		sigue.agregarSeguido(losiguen);
		losiguen.agregarSeguidor(sigue);
	}
	
	public void DejarSeguirAUsuario(String seguidor, String seguido){
		Usuario sigue=mu.findByNickname(seguidor); //esta persona ya no sigue al otro
		Usuario losiguen=mu.findByNickname(seguido); //a este no lo siguen
		sigue.quitarSeguido(seguido);
		losiguen.quitarSeguidor(seguidor);
	}
	
	/**
     * Crea una lista de produccion para el usuario recordado. Pre:Usuario ya recordado.
     */
	public void crearListaDeReproduccion(DtListaDeReproduccion lista){
		uRecordado.crearListaDeReproduccion(lista);
	}


}
