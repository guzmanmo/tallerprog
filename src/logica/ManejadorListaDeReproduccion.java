package logica;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ManejadorListaDeReproduccion {
	private static ManejadorListaDeReproduccion instancia = null;
	private Set<PorDefecto> listasPorDefecto;
	
	private ManejadorListaDeReproduccion() {
		listasPorDefecto = new HashSet<PorDefecto>();
	}
	public static ManejadorListaDeReproduccion getInstancia() {
		   if(instancia == null ) {
			   instancia = new ManejadorListaDeReproduccion();
		   }
		   return instancia;
	 }
	public ListaDeReproduccion seleccionarListaDeReproduccion (String name) {
	      Iterator<PorDefecto> it = this.listasPorDefecto.iterator();
	      while(it.hasNext()) {
	    	  PorDefecto listaDefecto = it.next();
	    	  if(listaDefecto.getNombre().equals(name));
	    	     return listaDefecto;
        }
	      return null;
	}
	public void ingresarListaPorDefecto(DtListaDeReproduccion lista) {
	      if(lista instanceof DtPorDefecto) {
	    	 PorDefecto nuevoDefecto = new PorDefecto(lista.getNombre(),lista.getTipo());
	    	 this.listasPorDefecto.add(nuevoDefecto);
        }
	    
	}
	
	public Set<DtListaDeReproduccion> listarSetDtListaDeReproduccion(){
	      Iterator<PorDefecto> it = this.listasPorDefecto.iterator();
	      Set<DtListaDeReproduccion> dtListas = new HashSet<DtListaDeReproduccion>();
	      while(it.hasNext()) {
	    	    PorDefecto infoLista = it.next();
	    	    DtListaDeReproduccion nuevoDato = infoLista.getDtListaDeReproduccion();
	    	    dtListas.add(nuevoDato);
	      }
	      return dtListas;
	}
 


}
