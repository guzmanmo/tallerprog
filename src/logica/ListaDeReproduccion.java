package logica;

import logica.Tipo;

import java.util.*;

public abstract class ListaDeReproduccion {
	
	   private String nombre;
	   private Tipo tipo;
	   private Set<Video> videos;
	   
	   public ListaDeReproduccion(String nombre , Tipo tipo) {
		   this.nombre=nombre;
		   this.tipo=tipo;
		   this.videos = new HashSet<Video>();
	   }
	   public String getNombre() {
		   return this.nombre;
	   }
	   public Tipo getTipo() {
		   return this.tipo;
	   } 
	   
	   public void setNombre(String nombre) {
		      this.nombre=nombre;
	   }
	   
	   public void setTipo(Tipo tipo) {
		   this.tipo=tipo;
	   }
	   public DtListaDeReproduccion getDtListaDeReproduccion() {
		     
		     DtListaDeReproduccion nuevaL = new DtListaDeReproduccion(this.nombre,this.tipo);
		     return nuevaL;    
	   }
	   
	   public Set<DtVideo> obtenerDtVideoDeLista(){
		      Set<DtVideo> dtVideos = new HashSet<DtVideo>();
		      Iterator<Video> it = this.videos.iterator();
		      while(it.hasNext()) {
		    	    Video infoVideo = it.next();
		    	    DtVideo nuevoDato = infoVideo.getDtVideo();
		    	    dtVideos.add(nuevoDato);
		      }  		
		      return dtVideos;
	   }
	   
	   public void removerVideo(DtVideo video) {
		      Iterator<Video> it = this.videos.iterator();
		      while(it.hasNext()) {
		    	  Video infoVideo = it.next();
		    	  if(infoVideo.getNombre().equals(video.getNombre()));
		    	     this.videos.remove(infoVideo); 
		      }
	   }
	   //DarVideos es lo mismo que obtenerDtVideoDeLista??
	         
}
