/**
 * 
 */
package logica;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import excepciones.NoExisteElementoException; 

/**
 * @author tpgr26 
 *
 */
public class Usuario {
	private String imagen;
	private Date fechaNacimiento;
	private String apellido;
	private String nombre;
	private String email; 
	private String nickName; 
	private Canal canal;
	
	//Colecciones
	private List<Comentario> comentarios;
	private List<Valoracion> valoraciones;
	private Map<String, Usuario> seguidores;
	private Map<String, Usuario> seguidos;
	
	
	/**
     * Constructor
     */
	public Usuario(DtUsuario u, DtCanal c){ 
		this.nombre=u.getNombre();
		this.apellido=u.getApellido();
		this.email=u.getEmail();
		this.imagen=u.getImagen();
		this.nickName=u.getNickName();
		this.fechaNacimiento=u.getFechaNacimiento();
		this.canal=new Canal(c.getNombre(),c.getDescripcion(),c.getTipo());
		comentarios=new ArrayList<Comentario>();
		valoraciones=new ArrayList<Valoracion>(); 
	}

	/**
     * Devuelve los datos de una lista de reproduccion y todos los videos que esta contiene */
	public DtListaDeReproduccionYVideos obtenerInfoListaDeReproduccion(DtListaDeReproduccion lista){
		try {
			return canal.obtenerInfoListaDeReproduccion(lista.getNombre());
		} catch (NoExisteElementoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	/**
     * Registra al usuario en el sistema*/
	public Set<DtVideo> darVideosEnLista(DtListaDeReproduccion lista){
		return canal.darVideosEnListaDeReproduccion(lista);
	}
	
	/**
     * Crea nueva lista de reproduccion */
	public void  crearListaDeReproduccion(DtListaDeReproduccion lista ){
		canal.crearListaDeReproduccion(lista);
	}
	public void agregarSeguido(Usuario seguido){
		seguidos.put(seguido.getNickName() ,seguido);
	}
	public void agregarSeguidor(Usuario  seguidor){
		seguidores.put(seguidor.getNickName() ,seguidor);
	}
	
	/** Deja de seguir al usuario*/
	public void quitarSeguido(String nickName){
		seguidos.remove(nickName);
	}
	/** Me dejan de seguir */
	public void quitarSeguidor(String  nickName){
		seguidores.remove(nickName);
	}
	public List<DtUsuario> listarSeguidos(){
		List<DtUsuario> coldtuser= new ArrayList<DtUsuario>();
		Iterator<Map.Entry<String, Usuario>> it= seguidos.entrySet().iterator();
		while(it.hasNext()) {
			Usuario u=(Usuario) it.next();
			DtUsuario dt= u.getDtUsuario();
			coldtuser.add(dt);
		}
		return coldtuser;
	}
	
	public List <DtUsuario> listarSeguidores(){
		List<DtUsuario> coldtuser= new ArrayList<DtUsuario>();
		Iterator<Map.Entry<String, Usuario>> it= seguidores.entrySet().iterator();
		while(it.hasNext()) {
			Usuario u=(Usuario) it.next();
			DtUsuario dt= u.getDtUsuario();
			coldtuser.add(dt);
		}
		return coldtuser;
	}
	
	public Set<DtListaDeReproduccion> listarDtListaDeReproduccion(){
		return canal.obtenerDtListaDeReproduccion();
	}
	
	public void  modificarUsuario(DtUsuario u){
		this.nombre=u.getNombre();
		this.apellido=u.getApellido();
		this.imagen=u.getImagen();
		this.fechaNacimiento=u.getFechaNacimiento();
	}
	public DtUsuario getDtUsuario(){
		return new DtUsuario(imagen,fechaNacimiento,apellido,nombre,email,nickName);
	}
	public List <DtVideo> listarDtVideo(){
		return canal.listarDtVideo();
	}
	public DtCanal getDtCanal(){
		return this.canal.getDtCanal();
	}
	public String getEmail(){ 
		return email;
	}
	public String getNickName(){ 
		return nickName;
	}

	public void modificarCanal(DtCanal canal) {
		canal.modificarCanal(canal);
		
	}
}
