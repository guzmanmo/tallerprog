/**
 * 
 */
package logica;

import java.util.Date;

/**
 * @author tpgr26 
 *
 */
public class DtUsuario {
	private String imagen;
	private Date fechaNacimiento;
	private String apellido;
	private String nombre;
	private String email; 
	private String nickName; 
	
	public DtUsuario(String imagen, Date fechaNacimiento, String apellido, String nombre, String email,
			String nickName){
		this.imagen=imagen;
		this.fechaNacimiento=fechaNacimiento;
		this.apellido=apellido;
		this.nombre=nombre;
		this.email=email;
		this.nickName=nickName;
	}
	
	public String getImagen(){ return imagen;}
	public Date getFechaNacimiento(){ return fechaNacimiento;}
	public String getApellido(){ return apellido;}
	public String getNombre(){ return nombre;}
	public String getEmail(){ return email;}
	public String getNickName(){ return nickName;}
	public String toString() {
	        return nickName +"-"+ email;
	}
	
}
